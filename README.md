composer install
copy .env.example .env
php artisan key:generate
php artisan jwt:secret
php artisan migrate
php artisan db:seed
php artisan db:seed --class=RoleTableSeeder
php artisan db:seed --class=UserTableSeeder

#credential login
email		: admin@gmail.com
password	: password

email		: user@gmail.com
password	: password

file postman: Test.postman_collection.json