<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use App\Models\User;
use App\Models\News;
use Illuminate\Support\Facades\Validator;
use Auth;

class NewsController extends Controller
{
    public function index()
    {
        $data = News::orderby('created_at', 'DESC')->paginate(15);

        return response()->json([
            'status' => 'success',
            'data' => $data
        ]);
    }

    public function create(Request $request)
    {
        $role = Auth::user()->get_role()->role;

        if ($role != 'Admin') {
            return response()->json(['message' => 'anda bukan Admin!']);
        }

        $validator = Validator::make(request()->all(),[
            'title' => 'required',
            'text'  => 'required',
        ]);

        if($validator->fails()){
            return response()->json($validator->messages());
        }

        $news = News::create([
            'title' => $request->title,
            'text'  => $request->text,
        ]);

        if ($news) {
            return response()->json(['message' => 'Penyimpanan berhasil!', 'data' => $news]);
        }else{
            return response()->json(['message' => 'Penyimpanan data gagal!']);
        }
    }

    public function update(Request $request)
    {
        $role = Auth::user()->get_role()->role;

        if ($role != 'Admin') {
            return response()->json(['message' => 'anda bukan Admin!']);
        }

        $validator = Validator::make(request()->all(),[
            'title' => 'required',
            'text'  => 'required',
        ]);

        if($validator->fails()){
            return response()->json($validator->messages());
        }

        $news = DB::table('news')->where('id', $request->id)->update([
            'title' => $request->title,
            'text'  => $request->text,
        ]);

        if ($news) {
            return response()->json(['message' => 'Update berhasil!']);
        }else{
            return response()->json(['message' => 'Update data gagal!']);
        }
    }

    public function delete(Request $request)
    {
        $news = DB::table('news')->where('id', $request->id)->delete();

        if ($news) {
            return response()->json(['message' => 'Deleted Successfully']);
        }else{
            return response()->json(['message' => 'Data tidak ditemukan']);
        }
    }
}
